pimcore.registerNS("pimcore.plugin.Kitt3nPimcoreRestrictionsBundle");

pimcore.plugin.Kitt3nPimcoreRestrictionsBundle = Class.create(pimcore.plugin.admin, {
    getClassName: function () {
        return "pimcore.plugin.Kitt3nPimcoreRestrictionsBundle";
    },

    initialize: function () {
        pimcore.plugin.broker.registerPlugin(this);
    },

    pimcoreReady: function (params, broker) {
        // alert("Kitt3nPimcoreRestrictionsBundle ready!");
    }
});

var Kitt3nPimcoreRestrictionsBundlePlugin = new pimcore.plugin.Kitt3nPimcoreRestrictionsBundle();
