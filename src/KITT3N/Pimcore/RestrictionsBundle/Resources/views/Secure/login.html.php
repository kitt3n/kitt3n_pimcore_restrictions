<?php
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

//$this->extend('layout.html.php');
$this->extend('Kitt3nPimcoreCustomBundle::layout.html.php');

/** @var \Symfony\Component\Form\FormView $form */
$form = $this->form;

?>

<div class="element element--form">
    <div class="grid-container c5-c7">
        <div class="column">

            <?php if ($this->error): ?>
                <div class="alert alert-danger"><?php echo $this->error->getMessage() ?></div>
            <?php endif; ?>

            <?php if ($this->oUser === null): ?>

                <?php
                $this->form()->setTheme($form, 'Kitt3nPimcoreRestrictionsBundle:Form/login');
                ?>

                <?= $this->form()->start($form); ?>
                <?= $this->form()->row($form['_username']) ;?>
                <?= $this->form()->row($form['_password']); ?>
                <?= $this->form()->widget($form['_target_path']) ;?>
                <br><br>
                <?= $this->form()->widget($form['_submit'], [
                    'attr' => [
                        'class' => ''
                    ]
                ]); ?>
                <?= $this->form()->end($form); ?>

            <?php else: ?>

            <?php endif; ?>

        </div>
    </div>
</div>
<script>
    var aJiroInputs = document.getElementsByClassName('input__field--jiro');
    var aJiroInputs__interval = setInterval(function () {
        for (var i = 0; i < aJiroInputs.length; i++) {
            var sValue = aJiroInputs[i].value;
            if (sValue.length > 0) {
                aJiroInputs[i].parentElement.setAttribute("class", "input input--jiro input--filled");
            } else {
                aJiroInputs[i].parentElement.setAttribute("class", "input input--jiro");
            }
        }
    }, 1000);
</script>

