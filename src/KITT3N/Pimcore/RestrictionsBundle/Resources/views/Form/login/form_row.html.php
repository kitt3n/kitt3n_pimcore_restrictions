<?php
$isValid = $form->vars['valid'];
?>

<span class="input input--jiro<?= $isValid ? '' : ' has-error' ?>">
    <?= $this->form()->widget($form, [
        'attr' => [
            'class' => 'input__field input__field--jiro'
        ]
    ]) ?>
    <label class="input__label input__label--jiro" for="<?= $form->vars['id'] ?>">
        <span class="input__label-content input__label-content--jiro"><?= $form->vars['label']; ?></span>
    </label>
</span>