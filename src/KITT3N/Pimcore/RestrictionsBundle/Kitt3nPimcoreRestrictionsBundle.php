<?php

namespace KITT3N\Pimcore\RestrictionsBundle;

use KITT3N\Pimcore\RestrictionsBundle\Tools\Installer;
use Pimcore\Extension\Bundle\AbstractPimcoreBundle;

class Kitt3nPimcoreRestrictionsBundle extends AbstractPimcoreBundle
{
    public function getJsPaths()
    {
        return [
            '/bundles/kitt3npimcorerestrictions/js/pimcore/startup.js'
        ];
    }

    public function getInstaller()
    {
        return $this->container->get(Installer::class);
    }
}
