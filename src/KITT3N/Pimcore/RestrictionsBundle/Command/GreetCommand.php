<?php

namespace KITT3N\Pimcore\RestrictionsBundle\Command;

use KITT3N\Pimcore\RestrictionsBundle\Kitt3nPimcoreRestrictionsBundle;
use Pimcore\Model\DataObject\User\Listing as UserListing;
use Pimcore\Model\DataObject\Group\Listing as GroupListing;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GreetCommand extends Command
{

    protected function configure(){
        $this->setName("kitt3npimcorerestrictionsbundle:greet")
            ->setDescription("Hello ")
            ->addArgument('name', InputArgument::REQUIRED, 'Who do you want to greet?')
            ->addArgument('last_name', InputArgument::OPTIONAL, 'Your last name?');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $output->writeln([
            '<info>Groups</>',
            '<info>==========================</>',
            '',
        ]);

        $aGroups = new GroupListing();
        foreach ($aGroups as $entry) {
            $output->writeln([
                $entry->getId()
            ]);
        }

        $output->writeln([
            '<info>Users</>',
            '<info>==========================</>',
            '',
        ]);

        $aUsers = new UserListing();
        foreach ($aUsers as $entry) {
            $output->writeln([
                $entry->getId() . ' - ' . $entry->getUsername()
            ]);
        }


        $output->writeln([
            '<info>Lorem Ipsum Dolor Sit Amet</>',
            '<info>==========================</>',
            '',
        ]);

        $text = 'Hi '.$input->getArgument('name');

        $lastName = $input->getArgument('last_name');
        if ($lastName) {
            $text .= ' '.$lastName;
        }

        $output->writeln($text.'!');

    }
}