<?php

namespace KITT3N\Pimcore\RestrictionsBundle\Controller;

use KITT3N\Pimcore\RestrictionsBundle\Form\LoginFormType;
use Pimcore\Config;
use Pimcore\Controller\FrontendController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Routing\Annotation\Route;

class SecureController extends FrontendController
{

    /**
     * @param Request $request
     * @param AuthenticationUtils $authenticationUtils
     * @Route(
     *     "/{_locale}/login",
     *     name="restrictions_login",
     *          defaults={
     *         "_locale": "de"
     *     }
     * )
     */
    public function loginAction(
        Request $request,
        AuthenticationUtils $authenticationUtils
    ) {

        $aReturn = $this->getBamParams($request);

        /* @var \Pimcore\Model\Document\Page $oBamLoginRedirect */
        $oBamLoginRedirect = $aReturn["oWebsiteConfig"]->get('bam.restrictions.login.redirect');

        switch (true) {
            case $oBamLoginRedirect instanceof \Pimcore\Model\Document\Page:
            case $oBamLoginRedirect instanceof \Pimcore\Model\Document\Link:
                $sBamLoginRedirect = $oBamLoginRedirect->getHref();
                break;
            default:
                $sBamLoginRedirect = "/" . $aReturn["sLocale"];
        }

        $this->view->oDocument = $aReturn["oDocument"];
        $this->view->aDocumentProperties = $aReturn["aDocumentProperties"];
        $this->view->oUser = $aReturn["oUser"];
        $this->view->oWebsiteConfig = $aReturn["oWebsiteConfig"];
        $this->view->bIsAllowed = $aReturn["bIsAllowed"];

        $this->view->aBamParams = $aReturn;

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();

        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        $formData = [
            '_username'    => $lastUsername,
            '_target_path' => $sBamLoginRedirect
        ];

        $form = $this->createForm(LoginFormType::class, $formData, [
            'action' => $this->generateUrl('restrictions_login'),
        ]);

        return [
            'form'            => $form->createView(),
            'error'           => $error,
        ];
    }


    /**
     * @param Request $request
     * @Route(
     *     "/{_locale}/logout",
     *     name="restrictions_logout",
     *     defaults={
     *         "_locale": "de"
     *     }
     * )
     */
    public function logoutAction(
        Request $request
    ) {

        $aReturn = $this->getBamParams($request);

        /* @var \Pimcore\Model\Document\Page $oBamLoginRedirect */
        $oBamLoginRedirect = $aReturn["oWebsiteConfig"]->get('bam.restrictions.login.redirect');

        switch (true) {
            case $oBamLoginRedirect instanceof \Pimcore\Model\Document\Page:
            case $oBamLoginRedirect instanceof \Pimcore\Model\Document\Link:
                $sBamLoginRedirect = $oBamLoginRedirect->getHref();
                break;
            default:
                $sBamLoginRedirect = "/";
        }

        $this->view->oDocument = $aReturn["oDocument"];
        $this->view->aDocumentProperties = $aReturn["aDocumentProperties"];
        $this->view->oUser = $aReturn["oUser"];
        $this->view->oWebsiteConfig = $aReturn["oWebsiteConfig"];
        $this->view->bIsAllowed = $aReturn["bIsAllowed"];

        $this->view->aBamParams = $aReturn;
    }

    public function getBamParams(Request $request)
    {
        /* @var \Pimcore\Model\Document\Page $oDocument */
        $oDocument = $this->document;
        /* @var array|null $xDocumentProperties */
        $xDocumentProperties = $oDocument->getProperties();
        /* @var array $aDocumentProperties */
        $aDocumentProperties = $xDocumentProperties === null ? [] : $xDocumentProperties;
        /** @var \Pimcore\Model\DataObject\User $oUser */
        $oUser = $this->getUser();

        $sLanguage = $oDocument->getProperty("language");
        $sLocale = $request->getLocale();
        if ( ! null == $sLocale) {
            $request->setLocale($sLocale);
        }

        /* @var \Pimcore\Config\Config $aWebsiteConfig */
        $oWebsiteConfig = Config::getWebsiteConfig($sLocale);

        /* @var bool $bIsAllowed */
        $bIsAllowed = true;

        return [
            "oDocument" => $oDocument,
            "aDocumentProperties" => $aDocumentProperties,
            "oUser" => $oUser,
            "oWebsiteConfig" => $oWebsiteConfig,
            "bIsAllowed" => $bIsAllowed,
            "aRestrictions" => [
                "aAssetRestrictions" => [],
                "bDocumentRestriction" => $bIsAllowed,
                "oUser" => $oUser,
                "oGroup" => null,
            ],
            "sLocale" => $sLocale,
            "sLanguage" => $sLanguage,
        ];

    }
}
