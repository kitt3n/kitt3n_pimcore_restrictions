<?php

namespace KITT3N\Pimcore\RestrictionsBundle\Tools;

use Doctrine\DBAL\Migrations\AbortMigrationException;
use Doctrine\DBAL\Migrations\Version;
use Doctrine\DBAL\Schema\Schema;
use Pimcore\Db\ConnectionInterface;
use Pimcore\Extension\Bundle\Installer\MigrationInstaller;
use Pimcore\Migrations\Migration\InstallMigration;
use Pimcore\Migrations\MigrationManager;
use Pimcore\Model\DataObject\ClassDefinition;
use Pimcore\Model\DataObject\ClassDefinition\Service;

use Symfony\Component\HttpKernel\Bundle\BundleInterface;

class Installer extends MigrationInstaller {

    /**
     * @var string
     */
    private $installSourcesPath;

    /**
     * @var array
     */
    private $classesToInstall = [
        'Group' => 'RestictionBundleGroup',
        'User' => 'RestictionBundleUser',
    ];

    public function __construct(
        BundleInterface $bundle,
        ConnectionInterface $connection,
        MigrationManager $migrationManager
    ) {
        $this->installSourcesPath = __DIR__ . '/../Resources/install';

        parent::__construct($bundle, $connection, $migrationManager);
    }

    public function migrateInstall(Schema $schema, Version $version)
    {
        /** @var InstallMigration $migration */
        $migration = $version->getMigration();
        if ($migration->isDryRun()) {
            $this->outputWriter->write('<fg=cyan>DRY-RUN:</> Skipping installation');

            return;
        }

//        $this->installFieldCollections();
        $this->installClasses();
//        $this->installObjectBricks();
//        $this->installTables($schema, $version);
//        $this->installTranslations();
//        $this->installPermissions();
    }

    public function migrateUninstall(Schema $schema, Version $version)
    {
        /** @var InstallMigration $migration */
        $migration = $version->getMigration();
        if ($migration->isDryRun()) {
            $this->outputWriter->write('<fg=cyan>DRY-RUN:</> Skipping uninstallation');

            return;
        }

//        $this->uninstallPermissions($version);
//        $this->uninstallTables($schema);
    }

    private function getClassesToInstall(): array
    {
        $result = [];
        foreach (array_keys($this->classesToInstall) as $className) {
            $filename = sprintf('class_%s_export.json', $className);
            $path = $this->installSourcesPath . '/classes/' . $filename;
            $path = realpath($path);

            if (false === $path || !is_file($path)) {
                throw new AbortMigrationException(sprintf(
                    'Class export for class "%s" was expected in "%s" but file does not exist',
                    $className,
                    $path
                ));
            }

            $result[$className] = $path;
        }

        return $result;
    }

    private function installClasses()
    {
        $classes = $this->getClassesToInstall();

        $mapping = $this->classesToInstall;

        foreach ($classes as $key => $path) {
            $class = ClassDefinition::getByName($key);

            if ($class) {
                $this->outputWriter->write(sprintf(
                    '     <comment>UPDATE:</comment> Class "%s".',
                    $key
                ));
            } else {
                $this->outputWriter->write(sprintf(
                    '     <comment>CREATE:</comment> Class "%s".',
                    $key
                ));
                $class = new ClassDefinition();
                $classId = $mapping[$key];

                $class->setName($key);
                $class->setId($classId);
            }

            $data = file_get_contents($path);
            $success = Service::importClassDefinitionFromJson($class, $data, false, true);

            if (!$success) {
                throw new AbortMigrationException(sprintf(
                    'Failed to create class "%s"',
                    $key
                ));
            }

        }

    }

}